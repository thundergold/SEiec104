/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seari.seiec104.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Properties;

/**
 *
 * @author sunzhu
 */
public class MiscTools
{
    static final char GOOD_CHAR=' ';
    static char[] BAD_CHARS={'\0','\udbc3','\udbc0'};
    
    /**
     * 读入配置文件，优先从当前工作路径读取，未找到则从classpath中读取
     * @param fileName
     * @return
     * @throws Exception 
     */
    public static Properties loadConfig(String fileName) throws Exception
    {
        File f = new File(fileName);
        InputStreamReader in ;
        if(f.exists())
           in = new InputStreamReader(new FileInputStream(fileName),"UTF-8");
        else
           in = new InputStreamReader(MiscTools.class.getClassLoader().getResourceAsStream(fileName),"UTF-8");
        Properties cfg = new Properties();
        cfg.load(in);
        in.close();
        return cfg;
    }
   

    
    /**
     * 判断当前操作系统是否是Windows
     * @return 
     */
    public static boolean isWindowsOs()
    {
        return System.getProperty("os.name").toLowerCase().startsWith("windows");
    }
    
    /**
     * 获取系统临时路径
     * @return 
     */
    public static String getSystemTempDir()
    {
        return System.getProperty("java.io.tmpdir");
    }

    
  
    /**
     * 从字符串解析时间
     * @param time
     * @param dftDateFormat
     * @return
     * @throws Exception 
     */
    public static Timestamp parseComplexTime(String time, DateFormat dftDateFormat) throws Exception
    {
        int p = time.indexOf("天前");
        int p1 = time.indexOf("前天");
        int p2 = time.indexOf("昨天");
        int p3 = time.indexOf("小时前");
        int p4 = time.indexOf("半小时前");
        int p5 = time.indexOf("分钟前");
        int p6 = time.indexOf("秒前");
        int p7 = time.indexOf("今天");
        if (p != -1)
        {//4 天前
            String d = time.substring(0, p).trim();
            String t = time.substring(p + 2).trim();
            Calendar rightNow = Calendar.getInstance();
            rightNow.add(Calendar.DATE, -1 * Integer.parseInt(d));
            String newTime = rightNow.get(Calendar.YEAR) + "-" + (rightNow.get(Calendar.MONTH) + 1) + "-" + rightNow.get(Calendar.DATE) + " 00:00";// + " " + t;
            return new Timestamp(dftDateFormat.parse(newTime).getTime());
        }
        else if (p1 != -1)
        {
            String t = time.substring(p1 + 2).trim();
            Calendar rightNow = Calendar.getInstance();

            rightNow.add(Calendar.DATE, -2);
            String newTime = rightNow.get(Calendar.YEAR) + "-" + (rightNow.get(Calendar.MONTH) + 1) + "-" + rightNow.get(Calendar.DATE) + " " + t;
            return new Timestamp(dftDateFormat.parse(newTime).getTime());
        }
        else if (p2 != -1)
        {
            String t = time.substring(p2 + 3).trim();
            Calendar rightNow = Calendar.getInstance();
            rightNow.add(Calendar.DATE, -1);
            String newTime = rightNow.get(Calendar.YEAR) + "-" + (rightNow.get(Calendar.MONTH) + 1) + "-" + rightNow.get(Calendar.DATE) + " " + t;
            return new Timestamp(dftDateFormat.parse(newTime).getTime());
        }
        else if (p4 != -1)
        {//半个小时前
            Calendar rightNow = Calendar.getInstance();
            rightNow.add(Calendar.MINUTE, -30);
            return new Timestamp(rightNow.getTime().getTime());
        }
        else if (p3 != -1)
        {//1 小时前
            String d = time.substring(0, p3).trim();
            Calendar rightNow = Calendar.getInstance();
            rightNow.add(Calendar.HOUR, -1 * Integer.parseInt(d));
            return new Timestamp(rightNow.getTime().getTime());
        }
        else if (p5 != -1)
        {//6 分钟前
            String d = time.substring(0, p5).trim();
            Calendar rightNow = Calendar.getInstance();
            rightNow.add(Calendar.MINUTE, -1 * Integer.parseInt(d));
            return new Timestamp(rightNow.getTime().getTime());
        }
        else if (p6 != -1)
        {//5 秒前
            String d = time.substring(0, p6).trim();
            Calendar rightNow = Calendar.getInstance();
            rightNow.add(Calendar.SECOND, -1 * Integer.parseInt(d));
            return new Timestamp(rightNow.getTime().getTime());
        }
        else if (p7 != -1)//今天 13:20
        {
            String t = time.substring(p7 + 2).trim();
            Calendar rightNow = Calendar.getInstance();
            String newTime = rightNow.get(Calendar.YEAR) + "-" + (rightNow.get(Calendar.MONTH) + 1) + "-" + rightNow.get(Calendar.DATE) + " " + t;
            return new Timestamp(dftDateFormat.parse(newTime).getTime());
        }
        else
        {
            return new Timestamp(dftDateFormat.parse(time).getTime());
        }
    }

    
    
    /**
     * 替换掉字符串中指定的字符为空格
     * @param s
     * @param c
     * @return 
     */
    private static String replaceBadChar(String s, char c)
    {
        if(s==null||s.length()==0)
            return s;
        int index;
        while((index=s.indexOf(c))>=0)
        {
            System.out.println("Found bad char at index: "+index);
            if(index==0)
                s=s.substring(1);
            else if(index==s.length()-1)
                s=s.substring(0,s.length()-1);
            else
                s=s.substring(0,index)+GOOD_CHAR+s.substring(index+1);
        }
        return s;
    }  
    
    /**
     * 替换掉字符串中postgresql不能插入的字符
     * @param s
     * @return 
     */
    public static String replaceBadChars(String s)
    {
        if(s==null||s.length()==0)
            return s;
        for(char c:BAD_CHARS)
            s=replaceBadChar(s,c);
        return s;
    }
    
    /**
     * 设定最大长度截取字符串
     * @param str
     * @param maxLength
     * @return 
     */
    public static String trimString(String str,int maxLength)
    {
        if(str==null)
            return null;
        return str.length()>maxLength?str.substring(0,maxLength-3)+"...":str;
    }
    
    /**
     * 当前线程暂停
     * @param mills 
     */
    public static void pause(long mills)
    {
        try
        {
            Thread.sleep(mills);
        }
        catch(InterruptedException ex)
        {
            
        }
    }
}
