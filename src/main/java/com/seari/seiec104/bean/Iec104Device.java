/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seari.seiec104.bean;

/**
 *
 * @author Rainbow
 */
public class Iec104Device
{
    String deviceName;
    String ip;
    boolean hasCounter = false;
    int port;
    int address;
    int stationID;

    public String getDeviceName()
    {
        return deviceName;
    }

    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getAddress()
    {
        return address;
    }

    public void setAddress(int address)
    {
        this.address = address;
    }

    public int getStationID()
    {
        return stationID;
    }

    public void setStationID(int stationID)
    {
        this.stationID = stationID;
    }

    public boolean getHasCounter()
    {
        return hasCounter;
    }

    public void setHasCounter(boolean hasCounter)
    {
        this.hasCounter = hasCounter;
    }
    
}
